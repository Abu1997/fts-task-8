import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any[],filterString:string,proName:string): any[] {

    const result = [];
    if(value.length === 0 || filterString === '' || proName === ''){
      return value;
    }
    
    for (const item of value){
      let x=item[proName];
      if (x){
        x =x.toString();
      }
      if(x && x.toLocaleLowerCase().includes(filterString.toLocaleLowerCase())){
        result.push(item);
      }
      
    }
    return result;
  }

}
