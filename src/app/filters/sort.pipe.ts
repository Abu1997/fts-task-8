import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(value:Array<string>, args:any[]): any[] {
    const sortForm = args[0];
    const sortField = args[1];
    let multipier = 0;
    if(sortField === 'desc'){
      multipier=-1;
    }

    value.sort((a:any,b:any) =>  {
      if(a[sortForm]<b[sortForm]){
        return -1 * multipier;
      }else if(a[sortForm]>b[sortForm]){
        return 1 * multipier;
      }else {
        return 0;
      }
    });
    return value; 
  }

}
