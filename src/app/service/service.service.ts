import { Injectable } from '@angular/core';
import {  HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http:HttpClient) { }

  urlErp (){
    return this.http.get<any>("https://erp.arco.sa:65//api/GetMyTicket?CustomerId=CIN0000150");
  }

urlContractlist() {
  return this.http.get<any>("https://erp.arco.sa:65/api/GetTicketCustomerDetails?CustomerId=CIN0000150");
}

urlLabourid(contractNo : any){
  let urlLabourID = "https://erp.arco.sa:65//api/GetTicketIndContractAllEmployee?CustomerId=CIN0000150&ContractId=";
  var labourList = urlLabourID + contractNo;
  return this.http.get<any>(labourList);
}
urlTicketType(){
  return this.http.get<any>( "https://erp.arco.sa:65//api/TickettypeList");
}
urlDepartmentType(ticketID:any){
  let  urlDepartment = "https://erp.arco.sa:65//api/GetTicketAssignedToGroupByTicketTypeId?TicketTypeID=";
  var Departmentlist = urlDepartment + ticketID;
  return this.http.get<any>(Departmentlist);
}
urlAssignto(){
  return this.http.get<any>("https://erp.arco.sa:65//api/assigntoList");
}
urlPriority(){
  return this.http.get<any>("https://erp.arco.sa:65//api/PriorityList");
}
urlCategorylist(priorityNo:any){
   let urlCategory = "https://erp.arco.sa:65//api/GetTicketGroupByDepatmentId?TicketAssignGroupId=";
   var categoryList = urlCategory + priorityNo;
   return this.http.get<any>(categoryList);
}
urlSubCategorylist(categoryNo:any){
  let urlSubCategory = "https://erp.arco.sa:65/api/SubGroupByGroup?id=";
  var subCategory = urlSubCategory + categoryNo;
  return this.http.get<any>(subCategory);
}




PostData(valid:any){
  let urlValidation = "https://erp.arco.sa:65//api/CreateTicketNew?UpdateTicketFields=";

  var tickvalid = JSON.stringify(valid);

  var urlSubmit = urlValidation + tickvalid

  return this.http.post<any>(urlSubmit,null);
}
}
