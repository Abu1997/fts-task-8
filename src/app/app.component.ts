import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { asapScheduler } from 'rxjs';
import { IcategoryType, IcontractNo, IemployeeId, IFeildsType, IsubCategoryType, TicketID } from './models/tickettype';
import { ServiceService } from './service/service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'task-combine';

  ticketList: TicketID[] = [];

  contractNumberList:IcontractNo[]=[];
  employeeId:IemployeeId[]=[];
  ticketType:IFeildsType[]=[];
  departmentId:IFeildsType[]=[];
  assignedTolist:IFeildsType[]=[];
  priorityId:IFeildsType[]=[];
  categoryList:IcategoryType[]=[];
  subCategory:IsubCategoryType[]=[];


  filterNumber = '';
  searchNumber = '';
  sortBy = 'asc';
  sortingFrom = '';
  pageStart:number=1;
  itemPerpage:number=5;
  showPopup=false;

  constructor (private fb:FormBuilder, private ticketAPIdata:ServiceService) { }

  ngOnInit() {
    this.getallTicket()
  }
  openPopup(){
    this.showPopup=true;
  }
  getallTicket(){
    this.ticketAPIdata.urlErp().subscribe(data => {
      console.log(data)
      this.ticketList=data.Data;
    })
  }

  page(){
    this.pageStart=1;
  }

  // ticketData = this.fb.group({

  //   contractNumber:['',Validators.required],
  //   employeeId:['',Validators.required],
  //   ticketType:['',Validators.required],
  //   departmentSection:['',Validators.required],
  //   assignedTo:['',Validators.required],
  //   priorityType:['',Validators.required],
  //   categoryId:['',Validators.required],
  //   subCategory:['',Validators.required],
  //   ticketSubject:['',Validators.required],
  //   description:['',Validators.required]
  // })


  // getallTicket(){
  //   this.ticketAPIdata.urlErp().subscribe(data => {
  //     this.ticketList=data.Data;
  //   })
  // }

  // getcontractList(){
  //   this.ticketAPIdata.urlContractlist().subscribe(data => {
  //     this.contractNumberList=data.Result2;
  //   })
  // }

  // getemployeeList(){
  //   let contractNumber = this.ticketData.value.contractNumber;
  //   this.ticketAPIdata.urlLabourid(contractNumber).subscribe(data => {
  //     this.employeeId=data;
  //   })
  // }

  // getticketType(){
  //   this.ticketAPIdata.urlTicketType().subscribe(data =>{
  //     this.ticketType=data.Data;
  //   })
  // }

  // getdepartmentList(){
  //   let ticketid = this.ticketData.value.ticketid;
  //   this.ticketAPIdata.urlDepartmentType(ticketid).subscribe(data => {
  //     this.departmentId=data;
  //   })
  // }

  // getAssignto(){
  //   this.ticketAPIdata.urlAssignto().subscribe(data =>{
  //     this.assignedTolist=data.Data;
  //   })
  // }

  // getpriorityList(){
  //   this.ticketAPIdata.urlPriority().subscribe(data =>{
  //     this.priorityId=data.Data;
  //   })
  // }

  // getcategoryList(){
  //   let categoryList = this.ticketData.value.categoryList;
  //   this.ticketAPIdata.urlCategorylist(categoryList).subscribe(data =>{
  //     this.categoryList=data;
  //   })
  // }

  // getsubCategory(){
  //   let subCategory = this.ticketData.value.subCategory;
  //   this.ticketAPIdata.urlSubCategorylist(subCategory).subscribe(data =>{
  //     this.subCategory=data;
  //   })
  // }

  // getsubmitData(){

  //   if(this.ticketData.valid){

  //     let forms = this.ticketData.value;

  //     let tickParams = {
  //       cusnam: "",
  //       custid: "CIN0000150",
  //       emil: "",
  //       descp: forms.description,
  //       contno: "",
  //       Page: "",
  //       PageSize: "",
  //       Priority: forms.priorityType,
  //       Group: forms.categoryId,
  //       SubGroup: forms.subCategory,
  //       Subject: forms.ticketSubject,
  //       AssignedTo: forms.assignedTo,
  //       TicketType: forms.ticketType,
  //       TicketAssignGroup: forms.departmentSection,
  //       ContractNumber: forms.contractNumber,
  //       LabourNumber: forms.employeeId,
  //       TicketChannel: 2,
  //       UserId: "cc.user",
  //       accuracy: "",
  //     };
  //     this.ticketAPIdata.PostData(tickParams).subscribe(data =>{

  //       alert(data)
  //       this.getallTicket();
  //     })
  //   }
  //   this.ticketData.reset();
  
  // }
  
  // filterText:any ={

  //   EnquiryId:null,
  //   CreatedDatetime:null,
  //   CreatedBy:null,
  //   ContractNumber:null,
  //   CustomerId:null,
  //   CustomerName:null,
  //   StatusName:null,
  //   TicketTypeId:null,
  //   PriorityName:null,
  //   Subject:null


  // }

  // filterCol={
  //   col : '',
  //   value : ''
  // }

  // filterValue(Column:any) {

  //   debugger;
  //   this.filterCol.col = Column;
  //   this.filterCol.value = this.filterText[Column] ; 

  // }
  // sort(form:any){
  //   this.sortingFrom = form;

  //   if(this.sortBy === 'desc'){
  //     this.sortBy = 'asc';
  //   }else{
  //     this.sortBy= 'desc';
  //   }
  // }
  filterText:any ={

    EnquiryId:null,
    CreatedDatetime:null,
    CreatedBy:null,
    ContractNumber:null,
    CustomerId:null,
    CustomerName:null,
    StatusName:null,
    TicketTypeId:null,
    PriorityName:null,
    Subject:null


  }

  filterCol={
    col : '',
    value : ''
  }

  filterValue(Column:any) {

    debugger;
    this.filterCol.col = Column;
    this.filterCol.value = this.filterText[Column] ; 

  }
  sort(form:any){
    this.sortingFrom = form;

    if(this.sortBy === 'desc'){
      this.sortBy = 'asc';
    }else{
      this.sortBy= 'desc';
    }
  }

}
