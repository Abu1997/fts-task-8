import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IcategoryType, IcontractNo, IemployeeId, IFeildsType, IsubCategoryType, TicketID } from '../models/tickettype';
import { ServiceService } from '../service/service.service';

@Component({
  selector: 'app-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.css']
})
export class PopUPComponent implements OnInit {
  ticketList: TicketID[] = [];

  contractNumberList:IcontractNo[]=[];
  employeeId:IemployeeId[]=[];
  ticketType:IFeildsType[]=[];
  departmentId:IFeildsType[]=[];
  assignedTolist:IFeildsType[]=[];
  priorityId:IFeildsType[]=[];
  categoryList:IcategoryType[]=[];
  subCategory:IsubCategoryType[]=[];

  constructor(private fb:FormBuilder, private ticketAPIdata:ServiceService) { }

  ngOnInit(): void {
    this.getcontractList();
    this.getticketType();
  }

  // openCreate(){
  //   if (!this.contractNumberList || this.contractNumberList.length < 1)
    
  //   this.getcontractList()

  // if (!this. ticketType|| this.ticketType.length < 1)
  //   this.getticketType()
  // }
  ticketData = this.fb.group({

    contractNumber:['',Validators.required],
    employeeId:['',Validators.required],
    ticketType:['',Validators.required],
    departmentSection:['',Validators.required],
    assignedTo:['',Validators.required],
    priorityType:['',Validators.required],
    categoryId:['',Validators.required],
    subCategory:['',Validators.required],
    ticketSubject:['',Validators.required],
    description:['',Validators.required]
  })


 

  getcontractList(){
    this.ticketAPIdata.urlContractlist().subscribe(data => {
      this.contractNumberList=data.Result2;
    })
  }

  getemployeeList(){
    let contractNumber = this.ticketData.value.contractNumber;
    this.ticketAPIdata.urlLabourid(contractNumber).subscribe(data => {
      this.employeeId=data;
    })
  }

  getticketType(){debugger
    this.ticketAPIdata.urlTicketType().subscribe(data =>{
      this.ticketType=data.Data;
    })
  }

  getdepartmentList(event : any){debugger
    let ticketid = this.ticketData.value.ticketType;
    this.ticketAPIdata.urlDepartmentType(ticketid).subscribe(data => {
      this.departmentId=data;
    })
  }

  getAssignto(){
    this.ticketAPIdata.urlAssignto().subscribe(data =>{
      this.assignedTolist=data.Data;
    })
  }

  getpriorityList(){
    this.ticketAPIdata.urlPriority().subscribe(data =>{
      this.priorityId=data.Data;
    })
  }

  getcategoryList(){
    let categoryList = this.ticketData.value.ticketType;
    this.ticketAPIdata.urlCategorylist(categoryList).subscribe(data =>{
      this.categoryList=data;
    })
  }

  getsubCategory(){
    debugger
    let subCategory = this.ticketData.value.categoryId;
    this.ticketAPIdata.urlSubCategorylist(subCategory).subscribe(data =>{
      this.subCategory=data;
    })
  }

  getsubmitData(){
    debugger

    if(this.ticketData.valid){

      let forms : any = this.ticketData.value;

      let tickParams = {
        cusnam: "",
        custid: "CIN0000150",
        Email: "",
        Description: forms.description,
        ContactNo: "",
        // Page: "",
        // PageSize: "",
        PriorityId: forms.priorityType,
        GroupId: forms.categoryId,
        SubGroupId: forms.subCategory,
        // Subject: forms.ticketSubject,
        TicketAssignGroupId: forms.assignedTo,
        TicketTypeId: forms.ticketType,
        TicketAssignGroup: forms.departmentSection,
        ContractNumber: forms.contractNumber,
        LabourNumber: forms.employeeId,
        TicketChannel: 2,
        UserId: "cc.user",
        Accuracy: "",
      };
      this.ticketAPIdata.PostData(tickParams).subscribe(data =>{
        debugger

        alert(data)
        // this.getallTicket();
      })
    }
    this.ticketData.reset();
  
  }
  
 

}
