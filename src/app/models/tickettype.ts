export interface TicketID {
    EnquiryId:number;
    CreatedDatetime:Date;
    CreatedBy:string;
    ContractNumber:string;
    CustomerId:string;
    CustomerName:string;
    StatusName:string;
    TicketTypeName:number;
    PriorityName:string;
    Subject:string;
}

export interface IcontractNo {
    ContractNumber: number;
}

export interface IemployeeId {
    EmployeeId: number;
}

export interface IFeildsType {
    ID: number;
    Name: string;
}

export interface IcategoryType {
    TicketGroupID: number;
    TicketGroupName: string;
}

export interface IsubCategoryType {
    Id: number;
    Description: string;
}